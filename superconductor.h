#ifndef SUPERCONDUCTOR_H
#define SUPERCONDUCTOR_H

#include "superconductor_global.h"

#include "qsslserver.h"

#include <QObject>
#include <QSslCertificate>
#include <QSslKey>
#include <QHash>

class QTcpSocket;
class QUdpSocket;
class QSettings;

struct Device {
  QSslCertificate certificate;
  QHostAddress address;
  uint port = 0;
  qulonglong nonce = 0;
  QSslSocket* socket = nullptr;
};

class SUPERCONDUCTORSHARED_EXPORT SuperConductor : public QObject
{
  Q_OBJECT
public:
  explicit SuperConductor(const QSslCertificate &certificate, const QSslKey &privateKey, const QString &casPath, uint port, uint udpPort, QObject *parent = 0);
  ~SuperConductor();

signals:
void availableDevice(QSslCertificate clientCert);
void receivedFromDevice(QSslCertificate clientCert, QByteArray data, QSslSocket* socket);

public slots:
  void start();
  void acceptDevice(QSslCertificate clientCert);
  void sendToDevice(QSslCertificate clientCert, QByteArray data);
  void announce(bool force = false);

protected slots:
  void incomingConnection();
  void onReadyRead();

private:
  void readSocketData(QAbstractSocket *socket);

  const QSslCertificate m_certificate;
  const QSslKey m_key;
  const QString m_casPath;
  const uint m_port, m_udpPort;

  QSslServer* m_secureServer;
  QUdpSocket* m_listeningSocket, *m_broadcastSocket;
  QHash<QByteArray, Device> m_knownDevices;
  qulonglong m_nonce;
};

#endif // SUPERCONDUCTOR_H
