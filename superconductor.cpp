#include "superconductor.h"

#include <QTcpServer>
#include <QSslSocket>
#include <QUdpSocket>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QSslKey>
#include <QCryptographicHash>
#include <QUuid>
#include <QHostInfo>
#include <QSettings>
#include <QTimer>

SuperConductor::SuperConductor(const QSslCertificate &certificate, const QSslKey &privateKey, const QString &casPath, uint port, uint udpPort, QObject *parent) :
  QObject(parent),
  m_certificate(certificate),
  m_key(privateKey),
  m_casPath(casPath),
  m_port(port),
  m_udpPort(udpPort),
  m_secureServer(nullptr),
  m_listeningSocket(nullptr),
  m_broadcastSocket(nullptr),
  m_nonce(0)
{
}

SuperConductor::~SuperConductor()
{
}

void SuperConductor::start()
{
  QSslConfiguration sslConfiguration;
  sslConfiguration.setLocalCertificate(m_certificate);
  sslConfiguration.setPrivateKey(m_key);
  sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyPeer);
//  sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
  sslConfiguration.setProtocol(QSsl::SecureProtocols);
  sslConfiguration.setCaCertificates(QSslCertificate::fromPath(m_casPath));
  qDebug() << "cas:" << sslConfiguration.caCertificates();

  m_secureServer = new QSslServer();
  m_secureServer->setSslConfiguration(sslConfiguration);
  connect(m_secureServer, &QSslServer::newConnection, this, &SuperConductor::incomingConnection);
  m_secureServer->listen(QHostAddress::Any, m_port);

  m_listeningSocket = new QUdpSocket();
  connect(m_listeningSocket, &QUdpSocket::readyRead, this, &SuperConductor::onReadyRead);
  m_listeningSocket->bind(QHostAddress::Any, m_udpPort);

  m_broadcastSocket = new QUdpSocket();

  for (int i = 0; i < 3; i ++)
    QTimer::singleShot(100 * (i+1), this, SLOT(announce()));
}

void SuperConductor::acceptDevice(QSslCertificate clientCert)
{
  qDebug() << "Accepting client";

  QFile casFile(m_casPath);
  if (casFile.open(QIODevice::Append)) {
    casFile.write(clientCert.toPem());
    casFile.close();
    m_secureServer->sslConfiguration().setCaCertificates(QSslCertificate::fromPath(m_casPath));
  }
  else
    qWarning() << "Could not write to cas file" << casFile.errorString();
}

void SuperConductor::sendToDevice(QSslCertificate clientCert, QByteArray data)
{
  if (m_knownDevices.contains(clientCert.toDer()))
  {
    Device &dev = m_knownDevices[clientCert.toDer()];

    if (dev.socket != nullptr)
    {
      qDebug() << "Sending to device (reusing socket):" << data;
      dev.socket->write(data);
    }
    else
    {
      QSslSocket *socket = new QSslSocket();
      QSslConfiguration sslConfiguration;
      sslConfiguration.setLocalCertificate(m_certificate);
      sslConfiguration.setPrivateKey(m_key);
      sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyPeer);
//      sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
      sslConfiguration.setProtocol(QSsl::SecureProtocols);
      sslConfiguration.setCaCertificates(QList<QSslCertificate>() << clientCert);

      socket->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
      socket->ignoreSslErrors(QList<QSslError>() << QSslError(QSslError::HostNameMismatch, clientCert));
      socket->setSslConfiguration(sslConfiguration);

      connect(socket, &QSslSocket::readyRead, this, &SuperConductor::onReadyRead);
      connect(socket, &QSslSocket::disconnected, [=]() {
        qDebug() << "Resetting device socket" << m_knownDevices.size();
        Device &dev = m_knownDevices[clientCert.toDer()];
        dev.socket->deleteLater();
        dev.socket = nullptr;
      });
      connect(socket, &QSslSocket::peerVerifyError, [](const QSslError & error) {
        qDebug() << "peer verify error:" << error.errorString();
      });
      connect(socket, &QSslSocket::stateChanged, [](QAbstractSocket::SocketState socketState) {
        qDebug() << "socket state changed:" << socketState;
      });
      connect(socket, &QSslSocket::encrypted, [=]() {
        qDebug() << "socket encryped, sending to device:" << data;
        QSslCertificate clientCert = socket->peerCertificate();
        if (m_knownDevices.value(clientCert.toDer()).socket == nullptr)
        {
          m_knownDevices[clientCert.toDer()].socket = socket;
        }
        socket->write(data);
      });
      socket->connectToHostEncrypted(dev.address.toString(), dev.port);
      qDebug() << "Connecting to device";
    }
  }
  else
    qDebug() << "Device currently not available";
}

void SuperConductor::announce(bool force)
{
  QByteArray servercert_der = m_certificate.toDer();

  if (force)
    m_nonce = 0;

  QJsonObject msg;
  msg["n"] = QString::number(m_nonce++);
  msg["cert"] = QString(servercert_der.toBase64());
  msg["port"] = QString::number(m_port);
  QJsonDocument doc(msg);
  m_broadcastSocket->writeDatagram(doc.toJson(), QHostAddress("255.255.255.255"), m_udpPort);
}

void SuperConductor::incomingConnection()
{
  QSslServer* server = qobject_cast<QSslServer*>(sender());

  QTcpSocket *socket = server->nextPendingConnection();
  if (!socket)
    return;

  socket->setSocketOption(QAbstractSocket::KeepAliveOption, 1);

  QSslSocket* secureSocket = qobject_cast<QSslSocket*>(socket);

  connect(secureSocket, &QSslSocket::readyRead, this, &SuperConductor::onReadyRead);
  connect(secureSocket, &QSslSocket::disconnected, [=]() {
    qDebug() << "Connection closed" << secureSocket->errorString();
    QSslCertificate clientCert = secureSocket->peerCertificate();
    if (m_knownDevices.value(clientCert.toDer()).socket == secureSocket)
    {
      m_knownDevices[clientCert.toDer()].socket = nullptr;
    }
    secureSocket->close();
    secureSocket->deleteLater();
  });
  connect(secureSocket, &QSslSocket::peerVerifyError, [](const QSslError & error) {
    qDebug() << "peer verify error:" << error.errorString();
  });
  connect(secureSocket, &QSslSocket::stateChanged, [](QAbstractSocket::SocketState socketState) {
    qDebug() << "socket state changed:" << socketState;
  });
  connect(secureSocket, &QSslSocket::encrypted, [=]() {
    qDebug() << "socket encryped";
    QSslCertificate clientCert = secureSocket->peerCertificate();
    if (m_knownDevices.value(clientCert.toDer()).socket == nullptr)
    {
      m_knownDevices[clientCert.toDer()].socket = secureSocket;
    }
  });
  if (socket->bytesAvailable() > 0)
    readSocketData(socket);
}

void SuperConductor::onReadyRead()
{
  QAbstractSocket* socket = qobject_cast<QAbstractSocket*>(sender());
  if (socket)
    readSocketData(socket);
}

void SuperConductor::readSocketData(QAbstractSocket* socket)
{
  QUdpSocket* broadcastSocket = qobject_cast<QUdpSocket*>(socket);
  QSslSocket* secureSocket = qobject_cast<QSslSocket*>(socket);
  if (broadcastSocket)
  {
    while (m_listeningSocket->hasPendingDatagrams()) {
      QByteArray datagram;
      datagram.resize(m_listeningSocket->pendingDatagramSize());
      QHostAddress sender;

      m_listeningSocket->readDatagram(datagram.data(), datagram.size(), &sender);

      QJsonParseError error;
      QJsonDocument doc = QJsonDocument::fromJson(datagram, &error);

      if (error.error == QJsonParseError::NoError)
      {
        QJsonObject msg = doc.object();

        QByteArray cert_data = QByteArray::fromBase64(msg.value("cert").toString().toLocal8Bit());
        qulonglong nonce = msg.value("n").toString().toULongLong(); // value("n").toInt() seem to be broken
        uint port = msg.value("port").toString().toULongLong();

        QSslCertificate clientCert = QSslCertificate(cert_data, QSsl::Der);

        if (!clientCert.isNull() && clientCert != m_certificate)
        {
          qDebug() << "Broadcast from" << sender.toString() << "nonce:" << nonce;
          bool firstTime = !m_knownDevices.contains(clientCert.toDer());
          Device &dev = m_knownDevices[clientCert.toDer()];

          if (nonce <= dev.nonce) // The device has re-broadcasted, possibly because of a restart
          {
            for (int i = 0; i < 3; i ++)
              QTimer::singleShot(100 * (i+1), this, SLOT(announce()));
          }

          dev.certificate = clientCert;
          dev.address = sender;
          dev.port = port;
          dev.nonce = nonce;
//          m_knownDevices[clientCert.toDer()] = dev;

          if (firstTime)
          {
            emit availableDevice(clientCert);
          }
        }
      }
      else
        qDebug() << error.errorString();
    }
    return;
  }
  else if (secureSocket)
  {
    QSslCertificate clientCert = secureSocket->peerCertificate();

    QByteArray data = secureSocket->readAll();

    QSslSocket *pluginSocket = nullptr;
    if (secureSocket != m_knownDevices.value(clientCert.toDer()).socket)
    {
      // We've got an additional connection from a device, since we already
      // have a connection, we pass this one on to whatever plugin the message
      // is addressed to.
      // Also we disconnect all signal/slots (except disconnect()) from it.
      disconnect(secureSocket, &QSslSocket::readyRead, 0, 0);
      pluginSocket = secureSocket;
      qDebug() << "Got additional connection" << clientCert;
    }
    else
    {
      qDebug() << "Command:" << data;
    }
    emit receivedFromDevice(clientCert, data, pluginSocket);
    return;
  }

  qDebug() << "Unhandled connection" << socket->readAll();
}
