#-------------------------------------------------
#
# Project created by QtCreator 2015-06-24T21:39:53
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = superconductor
TEMPLATE = lib
CONFIG += c++11

DEFINES += SUPERCONDUCTOR_LIBRARY

SOURCES += superconductor.cpp \
    qsslserver.cpp

HEADERS += superconductor.h\
        superconductor_global.h \
    qsslserver.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
